<?php
include_once("packagejson.class.php");
include_once(__DIR__.'/connectDB.class.php');

if ($_SERVER['REQUEST_METHOD'] != 'GET') {
    header('HTTP/1.1 405 Method Not Allowed');
    exit();
}

if (isset($_GET['type'])) {
    $rpc_o = new PackageJSON();
    echo $rpc_o->handle($_GET);
} else {
    //echo file_get_contents('../../doc/rpc.html');
    echo "<pre>
    type=search
        by :
            name
            name-desc
            status
            depends
            date        (builddate > à 2017-12-30)
        arg : chaine de recherche

    type=info
        arg[]= nom du paquet
        arg[]= nom du second paquet ...

    https://wiki.archlinux.org/index.php/AurJson
    </pre>
    ";
}
