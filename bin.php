#!/usr/bin/env php
<?php

// sudo chmod -R 777 /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/
// sudo chown -R patrick:http /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/

include('../config.php');

include_once(__DIR__.'/FileRepo.php');
include_once(__DIR__.'/DbRepo.php');
include_once(__DIR__.'/SqlRepo.php');
include_once(__DIR__.'/Compteur.php');

try {
    $dns = 'mysql:host='.$dbhost.';port=3306;dbname=manjaro_packages';
    $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $pdo = new PDO($dns, $dbuser, $dbpasswd, $arrExtraParam);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e) {
    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
    die($msg);
}

function update_branch() {
    // pacman -Fyy
    try {
        $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
        $dbr->unzip('stable',true);
    } catch (Exception $e) {
        // err: + de 132 Mo alloué
        trigger_error("ERREUR: ".$e->getMessage());
    }
    unset($dbr);
}

system('ls -l /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/');
update_branch();