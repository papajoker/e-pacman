#!/usr/bin/env php
<?php

/**
 * return packageName and version
 */
function getNameVersion($rep)
{
    $array= explode('-',$rep);
    $rev=array_pop($array);
    $v=array_pop($array);
    // some packageName have '-' in name as "zsh-lovers"
    return array( implode('-',$array), $v.'-'.$rev );
}

/**
 * test exact package file name, for exemple acpi_call -> acpi_call-dkms
 * return old version
 */
function packageIsUpdated($packageDetail,$path)
{
    $exists=glob("$path/${packageDetail[0]}-*",GLOB_ONLYDIR);
    if (count($exists)<1) return false; // new package
    foreach ( $exists as $d ){
        $dplus=getNameVersion(basename($d));
        if ($dplus[0]==$packageDetail[0]) {
            return $d;
        }
        else {
            echo "\nDEBUG: not same package but same prefix: ${dplus[0]}\n";
        }
    }
    return false;
}

$path=__DIR__.'/.store/stable/packages';
$fphar=".store/stable/community.files";
$i=0;
$ajax=false;

$p = new PharData($fphar);
try {
    foreach ($p as $fileinfo) {
        if ($fileinfo->isDir()) {
            //var_dump($fileinfo);
            $f=  $fileinfo->getFilename();
            $detail= getNameVersion($f);
            echo "\n${detail[0]} : ${detail[1]}\n";
            /**
             * if package + version in database // or if directory exist in packages/
             **/
            if (!file_exists($path.'/'.$f)) {

                $packageExist=packageIsUpdated($detail,$path);
                //extract $f/desc and $f/files
                //  $p->extractTo($path", $f.'/desc',true);
                //  $p->extractTo($path, $f.'/files',true);
                //  $f = new Filerepo($repo,detail[0]); ... as old version
                //  read 2 files and add(status=new) or update(status=updated) in DB

                if ($packageExist!==false){
                    echo "\t******* New version, found $packageExist\n";
                    // rmdir "$packageExist"
                    // db: set status=update
                }
                else {
                    echo "\t******* New package, not found ${detail[0]}-*\n";
                    // db: set status=new
                }
            }
            else {
                echo "no change \n";
                // db: set status=0
            }
            echo $fileinfo."\n\n";
            $i++;
            if ($ajax && $i>1000) {
                /**
                 * exit ajax for recall for begining only at lastfile+1
                 */
                return $f;
            }
        }
    }
    echo "\n $i packages \n\n ----------------- \n test extract only one package in ./zshdb-0.08-4\n";
    echo "\n extract zshdb-0.08-4 to ".__DIR__."\n";

    $ok = $p->extractTo(__DIR__."/zshdb-0.08-4",'zshdb-0.08-4',true);
    echo ($ok===true) ? "\n ok": "\n error";

    $ok = $p->extractTo(__DIR__."/",'zshdb-0.08-4/desc',true);
    echo ($ok===true) ? "\n ok": "\n error";
    
    $ok = $p->extractTo(__DIR__."/",'zshdb-0.08-4/files',true);
    echo ($ok===true) ? "\n ok": "\n error";

} catch (Exception $e) {
    echo $e->getMessage();
}