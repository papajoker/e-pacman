<?php

/*
** https://wiki.phpbb.com/Database_Abstraction_Layer

https://php.developpez.com/faq/?page=pdo
https://fmaz.developpez.com/tutoriels/php/comprendre-pdo/

*/

class SqlRepo
{
    public $db;
    public $repo ='core';
    private $consol = false;
    public $maxPackageTransaction = 300;

    public function __construct($consol=false)
    {
        $this->db = DB::connectDB();
        $this->consol = $consol;
        //if (!file_exists(__DIR__.'/.store/Sqlrepo.log')) file_put_contents(__DIR__.'/.store/Sqlrepo.log', '');
    }

    private function log($key, $data)
    {
        ;//file_put_contents(__DIR__.'/.store/Sqlrepo.log',"\n".$key.': '.print_r($data,true), FILE_APPEND);
    }

    public function transaction($path)
    {
        $i=0;
        $dir = new DirectoryIterator($path);
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                //$i++;
                /** pour tests **/
                //                if ($i>220) continue;
                $c = new Compteur();
                $p= new FileRepo($fileinfo->getFilename(), 'stable', $this->repo);
                $p->parse();
                if ($this->consol) {
                    echo "\n\nparse package:  ".$fileinfo->getFilename().' en '.$c->end()->value;
                }
                $c->start();
                $status=$this->inDB($p->fields['name'], $p->fields['version']);
                if ($this->consol) {
                    echo "\nget sql status ($status) package: ".$c->end()->value." ";
                }
                if ($status !=0) {
                    $c->start();
                    $this->addPackage($p, $status);
                    if ($this->consol) {
                        echo "\nget set sql package: ".$c->end()->value." ";
                    }
                    $i++;
                }
                set_time_limit(30);
            }
        }
        set_time_limit(300);
        return $i;
    }

    /**
     * reprendre après $lastfile
     */
    public function transactionFile($path, $lastfile='')
    {
        $i=0;
        $begin= ($lastfile!='') ? false : true;
        $this->log('lastfile:', $lastfile);
        $dir = new DirectoryIterator($path);
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                if (!$begin) {
                    if ($lastfile==$fileinfo->getFilename()) {
                        $begin=true;
                        $i=0;
                    }
                    continue;
                }
                
                $this->log('paquet:', $fileinfo->getFilename());
                $c = new Compteur();
                $p= new FileRepo($fileinfo->getFilename(), 'stable', $this->repo);
                $p->parse();
                if ($this->consol) {
                    echo "\n\nparse package:  ".$fileinfo->getFilename().' en '.$c->end()->value;
                }
                $c->start();
                $status=$this->inDB($p->fields['name'], $p->fields['version']);
                if ($this->consol) {
                    echo "\nget sql status ($status) package: ".$c->end()->value." ";
                }
                $i++;
                if ($status !=0) {
                    $c->start();
                    $this->addPackage($p, $status);
                    if ($this->consol) {
                        echo "\nget set sql package: ".$c->end()->value." ";
                    }
                    
                    $this->log('$i:', $i);
                }

                if ($i>$this->maxPackageTransaction) {
                    return $fileinfo->getFilename();
                }
                set_time_limit(30);
            }
        }
        //set_time_limit(300);
        return '';
    }

    // update or not ?
    public function inDB($package, $version)
    {
        $sql='SELECT status FROM packages WHERE name="'.$package.'" LIMIT 1;';
        if ($this->db->query($sql)->rowCount() >0) {
            $sql='SELECT status FROM packages WHERE name="'.$package.'" and version="'.$version.'" LIMIT 1;';
            if ($this->consol) {
                "<br />$sql";
            }
            if ($this->db->query($sql)->rowCount() >0) {
                $this->db->query('UPDATE packages SET status=0 WHERE name="'.$package.'";');
                //if ($this->consol) echo "$package $version inchangée \n";
                return 0;
            } else {
                //if ($this->consol) echo "$package nouvelle version ? $version \n";
                return 2;
            }
        }
        //if ($this->consol) echo "non présent: $package \n";
        return 1;
    }

    public function init()
    {
        $this->db->query('DELETE FROM packages WHERE status = "-1"');
        $this->db->query('UPDATE packages SET status="-1"');
    }

    public function close()
    {
        $this->db->query('OPTIMIZE TABLE packages');
        /*
        select count(status) from packages group by status;
        select count(status) from packages where status=1;
        select count(status) from packages where status=0;
        select count(status) from packages where status=-1;
        */
        if ($this->consol) {
            echo "\n\n------------ FINAL ------------";
            $row = $this->db->query('select count(status) from packages where status=1')->fetch();
            echo "\n".$row['count(status)']." status 1 (nouveaux)";
            $row = $this->db->query('select count(status) from packages where status=-1')->fetch();
            echo "\n".$row['count(status)']." status -1 (supprimés)";
            $row = $this->db->query('select count(status) from packages where status=0')->fetch();
            echo "\n".$row['count(status)']." status 0 (inchangés)";
            $row = $this->db->query('select count(status) from packages where status=2')->fetch();
            echo "\n".$row['count(status)']." status 2 (modifiés)";

            echo "\n";
            $row = $this->db->query('select count(status) from packages')->fetch();
            echo "\n".$row['count(status)']." paquets";
            $row = $this->db->query('select count(status) from packages where repo="core"')->fetch();
            echo "\n".$row['count(status)']." paquets core";
            $row = $this->db->query('select count(status) from packages where repo="extra"')->fetch();
            echo "\n".$row['count(status)']." paquets extra";
            $row = $this->db->query('select count(status) from packages where repo="community"')->fetch();
            echo "\n".$row['count(status)']." paquets community \n";
        }
    }


    private function sql_build_array($array, $type="UPDATE")
    {
        //Créer une chaine de place holder
        $arrPH = array();
        foreach ($array as $key=>$value) {
            $arrPH[] = "`$key`= :$key";
        }
        $strPH = implode(', ', $arrPH); //Contient: ?,?,?,?,?
        return $strPH;
    }

    public function addPackage($package, $insert=0)
    {
        $f=$package->fields;
        //$f['desc'] = $this->db->sql_escape($f['desc']);
        /*if (is_array($f['files']) && (count($f['files'])>255)) {
            $f['files'] = 'too big !!!';
        }*/
        // TODO
        // BUG
        // pas couper 255 char
        $f['depends'] = is_array($f['depends']) ? substr(implode(",", $f['depends']), 0, 255) : $f['depends'];
        /*
        $f['optdepends'] = is_array($f['optdepends']) ? substr(implode(",", $f['optdepends']), 0, 255) : $f['optdepends'];
        $f['makedepends'] = is_array($f['makedepends']) ? substr(implode(",", $f['makedepends']), 0, 255) : $f['makedepends'];
        $f['conflicts'] = is_array($f['conflicts']) ? substr(implode(",", $f['conflicts']), 0, 255) : $f['conflicts'];
        $f['provides'] = is_array($f['provides']) ? substr(implode(",", $f['provides']), 0, 255) : $f['provides'];
        $f['replaces'] = is_array($f['replaces']) ? substr(implode(",", $f['replaces']), 0, 255) : $f['replaces'];
        $f['license'] = is_array($f['license']) ? implode(",", $f['license']) : $f['license'];
        $f['files'] = is_array($f['files']) ? implode("\n", $f['files']) : $f['files'] ;
        */ /*
        $sql="UPDATE packages SET
                `name` = '${f['name']}',
                `base` = '${f['base']}',
                `version` = '${f['version']}',
                `desc` = '${f['desc']}',
                filename = '${f['filename']}',
                depends = '${f['depends']}',
                optdepends = '${f['optdepends']}',
                makedepends = '${f['makedepends']}',
                replaces = '${f['replaces']}',
                provides = '${f['provides']}',
                conflicts = '${f['conflicts']}',
                csize = '${f['csize']}',
                isize = '${f['isize']}',
                url = '${f['url']}',
                license = '${f['license']}',
                builddate = '${f['builddate']}',
                packager = '${f['packager']}',
                repo = '${f['repo']}',
                files = '${f['files']}',
                `status` = '1'
            ";*/

      
        
        if ($insert==2) {
            $f['status'] = 2;
            $sql = 'UPDATE packages SET ' . $this->sql_build_array($f, 'UPDATE');
            $sql.=" WHERE `name` = '${f['name']}' LIMIT 1";
        } else {
            $f['status'] = 1;
            $sql = 'INSERT INTO packages SET ' . $this->sql_build_array($f, 'INSERT');
        }
        $prep = $this->db->prepare($sql);
        //Associer les valeurs aux place holders
        foreach ($f as $key=>$value) {
            $prep->bindValue(":$key", $value, PDO::PARAM_STR);
        }

        try {
            $prep->execute();
        } catch (PDOException $e) {
            //TODO supp les echo
            echo "<br />$prep->queryString";
            echo "<br />".$prep->debugDumpParams();
            $msg = 'ERREUR PDO ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
            //throw $e;
            die($msg);
        }
        $prep->closeCursor();
        $prep = null;
        //$this->db->query($sql);
    }
}
