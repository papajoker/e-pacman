<?php

/**
Faire un champ state(activ,deleted,new) dans DB et/ou fichier dns_check_record

Avant update - marquer tous deleted
Lors maj
   if package exist "activ" else "new"
**/

include_once(__DIR__.'/FileRepo.php');
#include_once(__DIR__.'/SqlRepo.php');

class DbRepo
{
    private $maxRmDirectories = 500;
    protected $url='';
    private $ext='db';
    public $repos = array('core', 'extra', 'community'/*,'multilib'*/);

    public function __construct($url = 'https://mirror.netzspielplatz.de/manjaro/packages', $extension='db')
    {
        $this->url = $url;
        $this->ext = $extension;
        if (!file_exists(__DIR__.'/.store/DbRepo.log')) {
            file_put_contents(__DIR__.'/.store/DbRepo.log', '');
        }
    }

    private function log($key, $data)
    {
        file_put_contents(__DIR__.'/.store/DbRepo.log', "\n".$key.': '.print_r($data, true), FILE_APPEND);
    }

    public function isUpdated($branch)
    {
        $filename = __DIR__."/.store/$branch/state";
        $result = false;
        if (file_exists($filename)) {
            $result = preg_grep('/^state=.*/', file($filename));
            $result = explode('=', array_values($result)[0])[1];
            //var_dump($result);
        }
        file_put_contents($filename, file_get_contents($this->url."/$branch/state"));
        $new = preg_grep('/^state=.*/', file($filename));
        $new = explode('=', array_values($new)[0])[1];
        //var_dump($new);
        if ($result===false) {
            return true;
        }
        if ($result!=$new) {
            return true;
        }
        return false;
    }

    public function getUrl($branch = 'stable', $repo)
    {
        return $this->url.'/'.$branch."/$repo/x86_64/";
    }

    private function downloadFile($branch, $repo)
    {
        $url = $this->getUrl($branch, $repo)."$repo.".$this->ext;
        //echo "\n download: $url";
        $filename = __DIR__."/.store/$branch/".basename($url);
        file_put_contents($filename, file_get_contents($url));
        //echo "\n saved in: ".$filename."";
    }

    private function untarFile($branch, $repo)
    {
        $gz = __DIR__."/.store/$branch/$repo.".$this->ext;
        $tar = __DIR__."/.store/$branch/$repo.tar";
        if (file_exists("$tar")) {
            unlink("$tar");
        }
        echo "créer $tar depuis $gz";
        try {
            /*
             * memory error  : file to big
                $p = new PharData("$gz");
                //var_dump($p); exit;
                $p->decompress();
            */
            $f = fopen("$tar", 'w');
            $fz = gzopen("$gz", "r");
            while (!gzeof($fz)) {
                $data = gzread($fz, 102400);
                if (fwrite($f, $data)===false) {
                    fclose($f);
                    fclose($fz);
                    throw new Exception("ERREUR: write file: $gz in $tar");
                }
            }
            fclose($f);
            fclose($fz);
            unset($data);
        } catch (Exception $e) {
            trigger_error("ERREUR: ".$e->getMessage()." fichier: $gz");
        }
        unlink($gz);
    }

    private function nextRepo($lastRepo='')
    {
        if ($lastRepo=='') {
            return $this->repos[0];
        }
        $key = array_search($lastRepo, $this->repos);
        $key++;
        if (array_key_exists($key, $this->repos)===true) {
            return $this->repos[$key];
        }
        return '';
    }

    public function load($lastRepo = '')
    {
        $lastRepo=$this->nextRepo($lastRepo);
        if ($lastRepo !== '') {
            $this->downloadFile('stable', $lastRepo);
            $this->untarFile('stable', $lastRepo);
            return $lastRepo;
        }
        return '';
    }

    public function getPackageName($file)
    {
        $a= explode('-', "$file");
        array_pop($a);
        array_pop($a);
        return implode('-', $a);
    }

    private function rrmdir($src)
    {
        $count=0;
        if (!file_exists($src)) {
            return;
        }
        $dir = opendir($src);
        if ($dir===false) {
            throw new Exception("bad directory $src");
        }
        while (false !== ($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                $full = "$src/$file";
                //$this->log('dir:',$full);
                if (is_dir($full)) {
                    //$this->log('desc:',$full.'/desc');
                    unlink($full.'/desc'); //$this->log('erreur rm :',$full.'/desc');
                    if (file_exists($full.'/files')) {
                        unlink($full.'/files');
                    }
                    if (file_exists($full.'/.cache')) {
                        unlink($full.'/.cache');
                    }
                    if (rmdir($full)===false) {
                        throw new Exception("ERREUR: rmdir $file");
                    }
                    //$this->log('ERREUR rmdir : ',$full);
                    $count++;
                    if ($count>$this->maxRmDirectories) {
                        closedir($dir);
                        return $file;
                    }
                } else {
                    if (unlink($full)===false) {
                        throw new Exception("not removed $full");
                    }
                }
            }
        }
        closedir($dir);
        return '';
    }
    
    public function clearPackages()
    {
        $path=__DIR__."/.store/stable/packages";
        return $this->rrmdir($path);
        //if (!file_exists($path)) mkdir($path);
    }

    private function show_time($end, $begin)
    {
        $time=round($end-$begin);
        return ($time<60) ? $time." secondes" : round($time/60)." minutes";
    }

    public function unzip($lastRepo='', $consol=false)
    {
        $path = __DIR__."/.store/stable/packages";
        if (!file_exists($path)) {
            mkdir("$path");
        }

        $lastRepo=$this->nextRepo($lastRepo);
        if ($lastRepo!=='') {
            // untar
            $tar = __DIR__."/.store/stable/$lastRepo.tar";
            try {
                $phar = new PharData("$tar");
                $phar->extractTo("$path", null, true);

                /*foreach ($phar as $fileinfo) {
                    file_put_contents($path.'/'.$fileinfo->getFilename().'/desc', "%REPO%\n$lastRepo\n", FILE_APPEND);
                }*/
            } catch (Exception $e) {
                throw new Exception("ERREUR: ".$e->getMessage()." file: $tar in $path ");
            }
            //unlink ("$tar");
            //set_time_limit(1280);
            return $lastRepo;
        }
        return '';
    }

    public function setRepos($lastRepo='')
    {
        $path = __DIR__."/.store/stable/packages";
        if (!file_exists($path)) {
            mkdir("$path");
        }

        $lastRepo=$this->nextRepo($lastRepo);
        if ($lastRepo!=='') {
            // untar
            $tar = __DIR__."/.store/stable/$lastRepo.tar";
            try {
                $phar = new PharData("$tar");
                foreach ($phar as $fileinfo) {
                    file_put_contents($path.'/'.$fileinfo->getFilename().'/desc', "%REPO%\n$lastRepo\n", FILE_APPEND);
                }
            } catch (Exception $e) {
                throw new Exception("ERREUR: ".$e->getMessage()." file: $tar in $path ");
            }
            return $lastRepo;
        }
        return '';
    }

    public function setDatabase($lastfile='', $consol=false)
    {
        $path = __DIR__."/.store/stable/packages";
        $transaction= new SqlRepo($consol);
        //if ($consol) echo "\n SQL transaction :\n\n";
        if ($lastfile=='') {
            $transaction->init();
        }
        $file= $transaction->transactionFile($path, $lastfile);
        if ($file=='') {
            $transaction->close();
        }
        return $file;
    }

    public function getDep($dep = 'gtk3', $branch='stable')
    {
        $files=array();
        $path = __DIR__."/.store/$branch/packages/";
        $dir = new DirectoryIterator($path);
        foreach ($dir as $fileinfo) {
            if ($fileinfo->isDir() && !$fileinfo->isDot()) {
                $package= new FileRepo($fileinfo->getFilename());
                if ($package->getDep($dep)) {
                    $files[] = $package->package;
                }
            }
        }
        return $files;
    }
}
