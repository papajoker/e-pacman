<?php

class Compteur
{
    private $begin =0;
    private $end=0;
    public $value=0;

    public function __construct($start=true)
    {
        if ($start) {
            $this->start();
        }
    }

    public function start()
    {
        $this->end = 0;
        $this->value = 0;
        $this->begin = microtime(true);
    }

    public function end($brut=false)
    {
        $this->end = microtime(true);
        $this->value = ($this->end-$this->begin);
        return $this;
    }

    /**
     * retourne durée de l'action en secondes/minutes
     */
    public function str()
    {
        if ($this->end==0) {
            $this->end();
        }
        $time = round($this->value);
        return ($time<60) ? $time." secondes" : round($time/60)." minutes";
    }
}
