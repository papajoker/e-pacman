<?php

class DB
{
    private static $db = null;

    public static function connectDB()
    {
        global $dbhost;
        global $dbuser;
        global $dbpasswd;
        if (self::$db === null) {
            include_once("../config.php");
            if ($dbhost=='') {
                $dbhost='localhost';
            }
            try {
                $dns = 'mysql:host='.$dbhost.';port=3306;dbname=manjaro_packages';
                $dns = 'mysql:host='.$dbhost.';dbname=manjaro_packages';
                $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
                self::$db = new PDO($dns, $dbuser, $dbpasswd, $arrExtraParam);
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
                die($msg);
            }
        }
        return self::$db;
    }
}
