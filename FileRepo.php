<?php

class FileRepo
{
    protected $datas = array() ;
    public $fields = array(
        'name' => null,
        'base' => null,
        'version' => null,
        'desc' => null,
        //'filename' => null,
        'depends' => null,
        //'optdepends' => null,
        //'makedepends' => null,
        //'replaces' => null,
        //'provides' => null,
        //'conflicts' => null,
        'csize' => null,
        'isize' => null,
        //'url' => null,
        //'license' => null,
        'builddate' => null,
        //'packager' => null,
        'repo' => null,
        //'files' => null,
        'status' => 1,
    ) ;
    
    public function __construct($package, $branch = 'stable', $repo)
    {
        $this->branch = $branch;
        $this->package = $package;
        $this->path = __DIR__."/.store/$branch/packages/$package";
        $this->datas = array();
        $this->fields['repo'] = $repo;
    }
    
    public function parse()
    {
        $array=[];
        foreach ($this->fields as $key=> $value) {
            $array = $this->getDesc($key);
            if (count($array)>0) {
                if (count($array)==1) {
                    $this->fields[$key] = $array[0];
                    switch ($key) {
                        case 'builddate':
                            $this->fields[$key] = date('Y-m-d', $this->fields[$key]);
                            break;
                        /*case 'depends':
                        case 'optdepends':
                        case 'makedepends':
                        case 'files':
                            $this->fields[$key] = (string) implode(",", $this->fields[$key]);
                            break;*/
                        /*case 'csize':
                        case 'isize':
                            $this->fields[$key]=$this->formatBytes($this->fields[$key]);
                            break;*/
                    }
                } else {
                    $this->fields[$key] = $array;
                }
            }
        }

        return $this->fields;
        /*$files=file($this->path.'/files', FILE_SKIP_EMPTY_LINES);
        if (is_array($files)) {
            array_shift($files);
            foreach ($files as $value) {
                $value=trim($value);
                if (substr($value, -1)!='/') {
                    $this->fields['files'][]=$value;
                }
            }
        }*/
        //var_dump($this->fields);
        //exit;
    }

    public function getFiles()
    {
        $this->fields['files']=array();
        /*if (! file_exists($this->path.'/files')) {
            throw new Exception($this->path.'/files not exist');
        }*/
        if (file_exists($this->path.'/files')) {
            $files=file($this->path.'/files', FILE_SKIP_EMPTY_LINES);
            if (is_array($files)) {
                array_shift($files);
                foreach ($files as $value) {
                    $value=trim($value);
                    if (substr($value, -1)!='/') {
                        $this->fields['files'][]=$value;
                    }
                }
            }
        }
        return $this->fields['files'];
    }
    
    public function getDesc($key='VERSION')
    {
        $buffer = array();
        $found = false;
        $key = strtoupper($key);
        if (count($this->datas)<2) {
            if (! file_exists($this->path.'/desc')) {
                throw new Exception($this->path.'/desc not exist');
            }
            $this->datas = file($this->path."/desc");
        }
        reset($this->datas);
        
        foreach ($this->datas as $line) {
            $line = trim($line);
            if ("$line" == "%$key%") {
                $found = true;
                continue;
            }
            if ($found) {
                if ($line=='') {
                    return $buffer;
                }
                $buffer[] = $line;
            }
        }
        return $buffer;
    }

    public function getValue($key='VERSION')
    {
        $array = $this->getDesc($key);
        if (count($array)>0) {
            return $array[0];
        }
        return '';
    }
    
    public function valueExist($key= 'DEPENDS', $value = 'gtk3')
    {
        $buffer= $this->getDesc($key);
        foreach ($buffer as $item) {
            if (preg_match('/^'.$value.'/', $item)>0) {
                return true;
            }
        }
        return false;
    }
    
    public function getDep($depend = 'gtk')
    {
        return $this->valueExist('DEPENDS', $depend);
    }
    
    public function getVersion()
    {
        $array = $this->getDesc('VERSION');
        if (count($array)>0) {
            return $array[0];
        }
        return '';
    }
    
    public function getDate()
    {
        $d = $this->getDesc('BUILDDATE');
        if (is_array($d)) {
            $d= $d[0];
        }
        return date(DATE_RFC2822, $d);
    }
    
    private function formatBytes($size, $precision = 2)
    {
        $base = log($size, 1024);
        $suffixes = array('', 'Ko', 'Mo', 'G', 'T');
        return round(pow(1024, $base - floor($base)), $precision) .' '. $suffixes[floor($base)];
    }
    
    public function getSize()
    {
        $i = $this->getDesc('CSIZE');
        if (is_array($i)) {
            $i= $i[0];
        }
        return $this->formatBytes($i) . " installé: " . $this->formatBytes($this->getDesc('ISIZE')[0])."";
    }
}
