<?php
ob_start();
ini_set('max_execution_time', '1800');
set_time_limit(1800);

// sudo chmod -R 777 /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/
// sudo chown -R patrick:http /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/

header('Content-Type: application/json, charset=utf-8');
//include('../config.php');

include_once(__DIR__.'/connectDB.class.php');
include_once(__DIR__.'/FileRepo.php');
include_once(__DIR__.'/DbRepo.php');
include_once(__DIR__.'/SqlRepo.php');
include_once(__DIR__.'/Compteur.php');

class Actions
{
    private static $actions_range = array(
        'download','rm','unz','setrepos','sql'
    );

    protected $response = array(
        'action' =>'',
        'lastfile' => '',
        'msg' => '',
        'error' => 0,
        'end' => 0,
        'output' => ''
    );

    public function __construct()
    {
        if (! in_array($_GET['action'], self::$actions_range)) {
            $this->error = 'action not exist';
            $this->send();
        }
    }

    public function handle()
    {
        $action = $_GET['action'];
        $lastfile = $_GET['lastfile'];
        $this->response['action']=$action;
        call_user_func(array(&$this, $action), $lastfile);
        $this->send();
    }

    public function send()
    {
        $this->response['output'].=ob_get_clean();
        echo json_encode($this->response);
        exit;
    }

    public function sql($lastfile)
    {
        try {
            $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
            $this->response['lastfile']=$dbr->setDatabase($lastfile);
            $this->response['msg']="sql insert/update, last: ".$this->response['lastfile'];
            if ($this->response['lastfile']=='') {
                $this->response['end']=1;
                $this->response['action']='';
                $this->response['msg']="insert/update packages DB (by 200) --- END";
            }
        } catch (Exception $e) {
            $this->response['error']="ERREUR: ".$e->getMessage();
        }
        unset($dbr);
    }

    public function rm($lastfile)
    {
        try {
            $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
            $this->response['lastfile']=$dbr->clearPackages();
            $this->response['msg']='rm directories, last: '.$this->response['lastfile'];
            if ($this->response['lastfile']=='') {
                $this->response['end']=1;
                $this->response['action']='unz';
                $this->response['msg']='rm directories (by 500) --- END ';
            }
        } catch (Exception $e) {
            $this->response['error']="ERREUR: ".$e->getMessage();
        }
        unset($dbr);
    }

    public function download($lastfile)
    {
        try {
            $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
            $this->response['lastfile']=$dbr->load($lastfile);
            $this->response['msg']="download and un-tar, last: ".$this->response['lastfile'];
            if ($this->response['lastfile']=='') {
                $this->response['end']=1;
                $this->response['action']='rm';
                $this->response['msg']='download and un-tar --- END';
            }
        } catch (Exception $e) {
            $this->response['error']="ERREUR: ".$e->getMessage();
        }
        unset($dbr);
    }

    public function unz($lastfile)
    {
        try {
            $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
            $this->response['lastfile']=$dbr->unzip($lastfile);
            $this->response['msg']="un-gz, last: ".$this->response['lastfile'];
            if ($this->response['lastfile']=='') {
                $this->response['end']=1;
                $this->response['action']='setrepos';
                $this->response['msg']='un-gz --- END';
            }
        } catch (Exception $e) {
            $this->response['error']="ERREUR: ".$e->getMessage();
        }
        unset($dbr);
    }

    public function setrepos($lastfile)
    {
        try {
            $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
            $this->response['lastfile']=$dbr->setRepos($lastfile);
            $this->response['msg']="set repos, last: ".$this->response['lastfile'];
            if ($this->response['lastfile']=='') {
                $this->response['end']=1;
                $this->response['action']='sql';
                $this->response['msg']='set repos --- END';
            }
        } catch (Exception $e) {
            $this->response['error']="ERREUR: ".$e->getMessage();
        }
        unset($dbr);
    }
}

error_reporting(E_ERROR);
function exception_error_handler($severity, $message, $file, $line)
{
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");


$run = new Actions();
$run->handle();
