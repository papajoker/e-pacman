DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `name` varchar(64) NOT NULL,
  `base` varchar(64) DEFAULT NULL,
  `version` varchar(64) NOT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `filename` varchar(128) DEFAULT NULL,
  `depends` varchar(255) DEFAULT NULL,
  `csize` int(11) unsigned NOT NULL,
  `isize` int(11) unsigned NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `builddate` date NOT NULL,
  `repo` varchar(22) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '-1:del 1:new',
  PRIMARY KEY (`name`),
  KEY `version` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8; 
