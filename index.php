<?php
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();

/* ouvert a tous
//if ($user->data['user_id'] == ANONYMOUS)
if (
    ($user->data['group_id'] != 4) and
    ($user->data['group_id'] != 5)
    )
{
    trigger_error('NOT_AUTHORISED'); // https://wiki.phpbb.com/Function.trigger_error
}
*/

page_header('Paquets Manjaro');

$template->assign_vars(
     array(
    'PACKAGE'           => request_var('p', ''),
    'ARG'               => request_var('arg', 'i18n-'),
    'ARGINFO'           => request_var('arginfo', 'vlc'),
   )
);

$template->set_filenames(array(
    'body' => 'pacman.html',
));

make_jumpbox(append_sid("{$phpbb_root_path}index.$phpEx"));
page_footer();
