<?php

//include_once("aur.inc.php");
include_once(__DIR__.'/connectDB.class.php');
include_once('FileRepo.php');

/*
 * This class defines a remote interface for fetching data from the AUR using
 * JSON formatted elements.
 *
 * @package rpc
 * @subpackage classes
 */
class PackageJSON
{
    private $dbh = false;
    private $version = 1;
    private static $exposed_methods = array(
        'search', 'info', 'suggest'
    );
    private static $exposed_fields = array(
        'name', 'name-desc', 'status', 'depends', 'date'
    );
    private static $numeric_fields = array(
        'status'
    );
    private static $array_fields = array(
        'depends','optdepends','makedepends','files'
    );
    private static $status_values = array(
        'Supprimé', 'non modifié', 'Modifié', 'Ajouté'
    );


    public function __construct()
    {
        $this->dbh =  DB::connectDB();
        file_put_contents(__DIR__.'/.store/rpc.log', '');
    }

    private function log($key, $data)
    {
        file_put_contents(__DIR__.'/.store/rpc.log', "\n".$key.': '.print_r($data, true), FILE_APPEND);
    }

    /*
     * Handles post data, and routes the request.
     *
     * @param string $post_data The post data to parse and handle.
     *
     * @return string The JSON formatted response data.
     */
    public function handle($http_data)
    {
        /*
         * Unset global aur.inc.php Pragma header. We want to allow
         * caching of data in proxies, but require validation of data
         * (if-none-match) if possible.
         */
        header_remove('Pragma');
        /*
         * Overwrite cache-control header set in aur.inc.php to allow
         * caching, but require validation.
         */
        header('Cache-Control: public, must-revalidate, max-age=0');
        header('Content-Type: application/json, charset=utf-8');

        if (!isset($http_data['type']) || !isset($http_data['arg'])) {
            return $this->json_error('No request type/data specified.');
        }
        if (!in_array($http_data['type'], self::$exposed_methods)) {
            return $this->json_error('Incorrect request type specified.');
        }

        if (isset($http_data['search_by']) && !isset($http_data['by'])) {
            $http_data['by'] = $http_data['search_by'];
        }
        if (isset($http_data['by']) && !in_array($http_data['by'], self::$exposed_fields)) {
            return $this->json_error('Incorrect by field specified.');
        }

        //$this->dbh = DB::connect();
        $type = str_replace('-', '_', $http_data['type']);
        /*if ($type == 'info' && $this->version >= 5) {
            $type = 'multiinfo';
        }*/
        $json = call_user_func(array(&$this, $type), $http_data);

        $etag = md5($json);
        header("Etag: \"$etag\"");
        /*
         * Make sure to strip a few things off the
         * if-none-match header. Stripping whitespace may not
         * be required, but removing the quote on the incoming
         * header is required to make the equality test.
         */
        $if_none_match = isset($_SERVER['HTTP_IF_NONE_MATCH']) ?
            trim($_SERVER['HTTP_IF_NONE_MATCH'], "\t\n\r\" ") : false;
        if ($if_none_match && $if_none_match == $etag) {
            header('HTTP/1.1 304 Not Modified');
            return;
        }

        if (isset($http_data['callback'])) {
            $callback = $http_data['callback'];
            if (!preg_match('/^[a-zA-Z0-9()_.]{1,128}$/D', $callback)) {
                return $this->json_error('Invalid callback name.');
            }
            header('content-type: text/javascript');
            return '/**/' . $callback . '(' . $json . ')';
        } else {
            header('content-type: application/json');
            return $json;
        }
    }

    /*
     * Returns a JSON formatted error string.
     *
     * @param $msg The error string to return
     *
     * @return mixed A json formatted error response.
     */
    private function json_error($msg)
    {
        header('content-type: application/json');
        if ($this->version < 3) {
            return $this->json_results('error', 0, $msg, null);
        } elseif ($this->version >= 3) {
            return $this->json_results('error', 0, array(), $msg);
        }
    }

    /*
     * Returns a JSON formatted result data.
     *
     * @param $type The response method type.
     * @param $count The number of results to return
     * @param $data The result data to return
     * @param $error An error message to include in the response
     *
     * @return mixed A json formatted result response.
     */
    private function json_results($type, $count, $data, $error)
    {
        $json_array = array(
            'version' => $this->version,
            'type' => $type,
            'resultcount' => $count,
            'results' => $data
        );

        if ($error) {
            $json_array['error'] = $error;
        }

        return json_encode($json_array);
    }

    public function url_exists($url)
    {
        if (!$fp = curl_init($url)) {
            return false;
        }
        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_NOBODY, true);
        curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($handle, CURLOPT_TIMEOUT, 3); //timeout in seconds
        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);
        if ($httpCode >= 200 && $httpCode < 400) {
            return true;
        }
        return false;
    }

 
    /*
     * Retrieve package information (used in info, multiinfo, search and
     * depends requests).
     *
     * @param $type The request type.
     * @param $where_condition An SQL WHERE-condition to filter packages.
     *
     * @return mixed Returns an array of package matches.
     */
    private function process_query($type, $where_condition)
    {
        $max_results = 350;

        if ($this->version == 1) {
            $query = "SELECT * " .
                "FROM packages ".
                "WHERE ${where_condition} " .
                "LIMIT $max_results";
        }
        $result = $this->dbh->query($query);

        if ($result) {
            $resultcount = 0;
            $search_data = array();
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $resultcount++;

                if ($type == 'info') {
                    $folder= __DIR__.'/.store/stable/packages/'.$row['name'].'-'.$row['version'];
                    if (file_exists($folder.'/desc')) {
                        $this->log('exist', $folder);
                        $f= new FileRepo($row['name'].'-'.$row['version'], 'stable', '');
                        $row = $f->parse();
                        $row['url'] = $f->getValue('URL');
                        $row['filename'] = $f->getValue('FILENAME');
                        $row['packager'] = $f->getValue('PACKAGER');
                        $row['optdepends'] = $f->getDesc('OPTDEPENDS');
                        $row['files'] = $f->getFiles();

                        $url='https://raw.githubusercontent.com/manjaro/packages-'.$row['repo'].'/master/'.$row['name'].'/PKGBUILD';
                        if ($this->url_exists($url)) {
                            $row['PKGBUILD'] = $url;
                            $row['git'] = 'https://github.com/manjaro/packages-'.$row['repo'].'/tree/master/'.$row['name'];
                        }
                        $this->log('row:', $row);
                    } else {
                        $this->log('not found !', $folder);
                    }
                }

                foreach (self::$numeric_fields as $field) {
                    if (isset($row[$field])) {
                        $row[$field] = intval($row[$field]);
                    }
                }
                foreach (self::$array_fields as $field) {
                    if (isset($row[$field]) && is_array($row[$field]) && count($row[$field])>0) {
                        //$this->log('array to convert',$row[$field]);
                        //$sep= ($field=='files') ? '<br />' : ', ';
                        $sep='<br />';
                        $row[$field] = '<br /><div class="package_array">'.implode($sep, $row[$field]).'</div>';
                    } else {
                        $row[$field]='';
                    }
                }

                //$i = $row['status']+1;
                $row['status'] = self::$status_values[ $row['status']+1 ];

                /*if ($type == 'info') {
                    $search_data = $row;
                    break;
                } else {
                    array_push($search_data, $row);
                }*/
                array_push($search_data, $row);
            }

            if ($resultcount === $max_results) {
                return $this->json_error('Too many package results.');
            }

            return $this->json_results($type, $resultcount, $search_data, null);
        } else {
            return $this->json_results($type, 0, array(), null);
        }
    }

    /*
     * Parse the args to the multiinfo function. We may have a string or an
     * array, so do the appropriate thing. Within the elements, both * package
     * IDs and package names are valid; sort them into the relevant arrays and
     * escape/quote the names.
     *
     * @param array $args Query parameters.
     *
     * @return mixed An array containing 'ids' and 'names'.
     */
    private function parse_multiinfo_args($args)
    {
        if (!is_array($args)) {
            $args = array($args);
        }

        $id_args = array();
        $name_args = array();
        foreach ($args as $arg) {
            if (!$arg) {
                continue;
            }
            $name_args[] = $this->dbh->quote($arg);
        }

        return array('ids' => $id_args, 'names' => $name_args);
    }

    /*
     * Performs a fulltext mysql search of the package database.
     *
     * @param array $http_data Query parameters.
     *
     * @return mixed Returns an array of package matches.
     */
    private function search($http_data)
    {
        $keyword_string = $http_data['arg'];

        if (isset($http_data['by'])) {
            $search_by = $http_data['by'];
        } else {
            $search_by = 'name-desc';
        }

        if ($search_by === 'name' || $search_by === 'name-desc') {
            if (strlen($keyword_string) < 2) {
                return $this->json_error('Query arg too small.');
            }
            $keyword_string = $this->dbh->quote("%" . addcslashes($keyword_string, '%_') . "%");

            if ($search_by === 'name') {
                $where_condition = "(name LIKE $keyword_string)";
            } elseif ($search_by === 'name-desc') {
                $where_condition = "(name LIKE $keyword_string OR `desc` LIKE $keyword_string)";
            }
        } elseif ($search_by === 'status') {
            if (empty($keyword_string)) {
                $where_condition = " status = 2 ";
            } else {
                $keyword_string = (int) $keyword_string;
                $where_condition = "status = $keyword_string ";
            }
        } elseif ($search_by === 'depends') {
            if (empty($keyword_string)) {
                return $this->json_error('Query arg too small.');
            } else {
                $keyword_string_sub = $this->dbh->quote($keyword_string);
                $keyword_string = $keyword_string_sub.'|^'.$keyword_string_sub.'[,>=]|,'.$keyword_string_sub.'$|,'.$keyword_string_sub.'[,>=]';
                $where_condition = "(depends RLIKE '$keyword_string' ";
            }
        } elseif ($search_by === 'date') {
            if (empty($keyword_string)) {
                $where_condition = " builddate > DATE_SUB(NOW(), INTERVAL 7 DAY) ";
            } else {
                $keyword_string = (int) $keyword_string;
                $where_condition = "builddate > $keyword_string ";
            }
        }
        if ($search_by != 'status') {
            $where_condition .= ' AND status>-1 ';
        }
        return $this->process_query('search', $where_condition);
    }

    /*
     * Returns the info on a specific package.
     *
     * @param array $http_data Query parameters.
     *
     * @return mixed Returns an array of value data containing the package data
     */
    private function info($http_data)
    {
        $pqdata = $http_data['arg'];
        $where_condition = "name = " . $this->dbh->quote($pqdata);

        return $this->process_query('info', $where_condition);
    }


    /*
     * Get all package names that start with $search.
     *
     * @param array $http_data Query parameters.
     *
     * @return string The JSON formatted response data.
     */
    private function suggest($http_data)
    {
        $search = $http_data['arg'];
        $query = "SELECT name FROM packages ";
        $query.= "WHERE name LIKE ";
        $query.= $this->dbh->quote(addcslashes($search, '%_') . '%');
        $query.= "ORDER BY name ASC LIMIT 20";

        $result = $this->dbh->query($query);
        $result_array = array();

        if ($result) {
            $result_array = $result->fetchAll(PDO::FETCH_COLUMN, 0);
        }

        return json_encode($result_array);
    }
}
