<?php
error_reporting(E_ALL & ~ E_WARNING);
// http://www.fox-infographie.com/wblog_ajax_lance_php.php
/*
*  sudo rm -rf ~/workspace/Manjaro/manjaro.fr/phpBB/cache/production/*
*  sudo chown -R http:http /home/Data/Patrick/workspace/Manjaro/php-pacman/.store/
*/
define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : '../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);
include($phpbb_root_path . 'common.' . $phpEx);
include($phpbb_root_path . 'config.php');

/*
foreach(get_loaded_extensions() as $extension)
{
    if(strpos(strtolower($extension), 'pdo') !== FALSE)
    {
        echo $extension.'<br/>';
    }
}*/

global $pdo;
if ($dbhost=='') {
    $dbhost='localhost';
}
try {
    $dns = 'mysql:host='.$dbhost.';port=3306;dbname=manjaro_packages';
    $dns = 'mysql:host='.$dbhost.';dbname=manjaro_packages';
    $arrExtraParam= array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $pdo = new PDO($dns, $dbuser, $dbpasswd, $arrExtraParam);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    $msg = 'ERREUR PDO dans ' . $e->getFile() . ' L.' . $e->getLine() . ' : ' . $e->getMessage();
    die($msg);
}

include_once(__DIR__.'/FileRepo.php');
include_once(__DIR__.'/DbRepo.php');
include_once(__DIR__.'/SqlRepo.php');
include_once(__DIR__.'/Compteur.php');

// Start session management
$user->session_begin();
$auth->acl($user->data);
$user->setup();

error_reporting(E_ALL & ~ E_WARNING);

//if ($user->data['user_id'] == ANONYMOUS)
if (
    ($user->data['group_id'] != 4) and
    ($user->data['group_id'] != 5)
    ) {
    trigger_error('NOT_AUTHORISED'); // https://wiki.phpbb.com/Function.trigger_error
}

page_header('e-pacman');
$template->assign_vars(
    array(
    'TOP_TITLE'               => 'e-pacman',
   )
);

if (request_var('clear', '0')=='1') {
    $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
    $dbr->clearPackages();
    $template->assign_vars(array( 'ACTU' => "packages vidés"));
}

$download_branch = request_var('download', '');
if ($download_branch !='') {
    // pacman -Fyy
    $template->assign_vars(array( 'SUB_TITLE' => "download"));
    try {
        $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
        $dbr->load('stable');
        $template->assign_vars(array( 'ACTU' => 'Stable'." uploaded"));
    } catch (Exception $e) {
        // err: + de 132 Mo alloué
        trigger_error("ERREUR: ".$e->getMessage());
    }
    unset($dbr);
}

$update_branch = request_var('update', '');
if ($update_branch !='') {
    // pacman -Fyy
    $template->assign_vars(array( 'SUB_TITLE' => "update"));
    try {
        $dbr = new DbRepo('https://mirror.netzspielplatz.de/manjaro/packages');
        $dbr->unzip($update_branch);
        $template->assign_vars(array( 'ACTU' => $update_branch." db updated"));
    } catch (Exception $e) {
        // err: + de 132 Mo alloué
        trigger_error("ERREUR: ".$e->getMessage());
    }
    unset($dbr);
}

$action = request_var('action', '');

if ($action == 'package') {
    $package = request_var('package_name', '');
    $template->assign_vars(array(
        'SUB_TITLE' => "différences stable/testing sur le paquet $package",
        'PACKAGE_NAME' => $package,
    ));
    $s= new FileRepo($package, 'stable');
    $t= new FileRepo($package, 'testing');

    $tmp = $s->getDesc('DESC');
    $template->assign_block_vars('package', array(
        'LABEL' => $package,
        'VALUE' => $tmp[0],
    ));
    $tmp = $s->getDesc('REPO');
    $template->assign_block_vars('package', array(
        'LABEL' => "Dépôt",
        'VALUE' => $tmp[0],
    ));
    $template->assign_block_vars('package', array(
        'LABEL' => "Version(s)",
        'VALUE' => '<li title="stable">'.$s->getVersion().'</li><li title="testing">'.$t->getVersion().'</li>',
    ));
    $template->assign_block_vars('package', array(
        'LABEL' => "Date",
        'VALUE' => '<li title="stable">'.$s->getDate().'</li><li title="testing">'.$t->getDate().'</li>',
    ));
    $template->assign_block_vars('package', array(
        'LABEL' => "Taille(s)",
        'VALUE' => '<li title="stable">'.$s->getSize().'</li><li title="testing">'.$t->getSize().'</li>',
    ));
}

if ($action == 'dep') {
    $package = request_var('package_name', '');
    $template->assign_vars(array(
        'SUB_TITLE' => "Dépendances de $package dans stable",
        'PACKAGE_NAME' => $package,
    ));
    $dbr = new DbRepo();
    $datas = $dbr->getDep($package, 'stable');
    foreach ($datas as $value) {
        $template->assign_block_vars('package', array(
            'LABEL' => $value,
            'VALUE' => '',
        ));
    }
    unset($dbr);
    unset($datas);
}

if ($action == 'branch') {
    $package= request_var('package_name', '');
    $template->assign_vars(array( 'SUB_TITLE' => "différences entre stable/testing"));
    $path_s = __DIR__."/.store/stable/packages";
    $path_t = __DIR__."/.store/testing/packages";

    $dir_t = new DirectoryIterator($path_t);
    foreach ($dir_t as $fileinfo) {
        if ($fileinfo->isDir() && !$fileinfo->isDot()) {
            $array_t[]=$fileinfo->getFilename();
        }
    }

    $dir_s = new DirectoryIterator($path_s);
    foreach ($dir_s as $fileinfo) {
        if ($fileinfo->isDir() && !$fileinfo->isDot()) {
            $array_s[]=$fileinfo->getFilename();
        }
    }

    //echo "pas encore présent dans stable, pour bientot ☺ \n";
    $template->assign_vars(array( 'ACTU' => "pas encore présent dans stable, pour bientot"));
    $datas = array_diff($array_t, $array_s);
    unset($array_t);
    unset($array_s);
    foreach ($datas as $value) {
        $template->assign_block_vars('package', array(
            'LABEL' => $value,
            'VALUE' => '',
        ));
    }
}


$template->set_filenames(array(
    'body' => 'pacman_admin.html',
));

//make_jumpbox(append_sid("{$phpbb_root_path}index.$phpEx"));
page_footer();
